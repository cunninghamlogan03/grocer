import { useEffect, useState } from 'react';
import './App.css';
import Recipe from './components/RecipeForm/Recipe';
import { IngredientType, RecipeType } from './Types/Ingredient';
import Ingredient from './components/Ingredient/Ingredient';
import AddGrocery from './components/AddGrocery/AddGrocery';

// have option to select department type (produce, meat, etc)
// sort final list by department type

// have completerecipe button pass recipe into main component
// be able to select recipes to add to shopping list

// consildate similar recipe items

// ability to save/share lists

// make it not ugly

function App() {
  const [isAddingRecipe, setIsAddingRecipe] = useState(false)
  const [groceryList, setGroceryList] = useState<IngredientType[]>([])
  const [recipeList, setRecipeList] = useState<RecipeType[]>([])

  const addRecipeToList = (recipe: RecipeType) => {
    setRecipeList([...recipeList, recipe])
    setIsAddingRecipe(false)

    setTimeout(() => consolidateGroceries(), 250)
  }

  const addGroceryToList = (groceryName: string, groceryQuantity: string) => {
    const newGrocery: IngredientType = { name: groceryName, quantity: groceryQuantity }
    setGroceryList([...groceryList, newGrocery])

    consolidateGroceries()
  }

  const consolidateGroceries = () => {
    let consolidatedList: IngredientType[] = []

    recipeList.map(r => r.ingredients.map(i => {
      if (consolidatedList.some(e => e.name === i.name)) {
        // consolidatedList[i.name] += 
      }
    }))
  }

  return (
    <div className="App">
      <header>
        <div className='logo'>
          <img src="" alt="GetchaGroceries :)" />
        </div>

        <h1>GrocerList+</h1>
      </header>

      <div className='content'>
        <div className='grocery-list'>
          Current Grocery List

          {/* {groceryList.map(g => <Ingredient name={g.name} quantity={g.quantity} key={g.name}/>)} */}

          {recipeList.map(r => r.ingredients.map(i => <Ingredient name={i.name} quantity={i.quantity} />))}
          {/* {recipeList.map(i => <p>{i[0].name}</p>)} */}
          {/* {testList.map(i => <p>{i} - Test Item</p>)} */}
        </div>

        <div>
          <div className='add-grocery'>
            <AddGrocery addGrocery={addGroceryToList} />
            {isAddingRecipe ? 
              <button onClick={() => { setIsAddingRecipe(false) }}>Cancel Recipe</button> :
              <button onClick={() => { setIsAddingRecipe(true) }}>Add Recipe</button>}
          </div>

          <div className='recipe-group'>
            <div className='add-recipe'>

              {isAddingRecipe ?
                <Recipe onFinishRecipe={addRecipeToList} /> : null}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
