export interface IngredientType {
    name: string,
    quantity: string
}

export interface RecipeType {
    ingredients: IngredientType[]
}