import { useState } from "react";
import { IngredientType, RecipeType } from '../../Types/Ingredient'
import './Recipe.css'
import Ingredient from "../Ingredient/Ingredient";
import AddGrocery from "../AddGrocery/AddGrocery";

interface RecipeProps {
    onFinishRecipe: (recipe: RecipeType) => void
}

const Recipe = (props: RecipeProps) => {
    let [recipeIngredients, setRecipeIngredients] = useState<IngredientType[]>([])
    let name = "";
    let quantity = "";

    const resetComponent = () => {
        name = ""
        quantity = ""
        setRecipeIngredients([])
    }

    const completeRecipe = () => {
        const finalRecipe: RecipeType = { ingredients: recipeIngredients }
        props.onFinishRecipe(finalRecipe)
        resetComponent()
    }

    const addGroceryToList = (groceryName: string, groceryQuantity: string) => {
        const newGrocery: IngredientType = { name: groceryName, quantity: groceryQuantity }
        setRecipeIngredients([...recipeIngredients, newGrocery])

        name = ""
        quantity = ""
    }

    return (
        <>
            <AddGrocery addGrocery={addGroceryToList} completeRecipe={completeRecipe} />
            <button onClick={completeRecipe}>Complete Recipe</button>

            <div id="currentIngredients">
                <p>Current Recipe Ingredients:</p>
                {recipeIngredients?.map(i => <Ingredient name={i.name} quantity={i.quantity} key={i.name}/>)}
            </div>
        </>
    );
};

export default Recipe;