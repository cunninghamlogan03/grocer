import './Ingredient.css'

interface IngredientProps {
    name: string,
    quantity: string
}

const Ingredient = (props: IngredientProps) => {
    return (
        <div className="ingredient">
            <p>{props.name}</p>
            <p>{props.quantity}</p>
        </div>
    )
}

export default Ingredient