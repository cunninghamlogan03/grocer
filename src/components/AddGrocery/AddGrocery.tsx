interface AddGroceryProps {
    addGrocery: (goceryName: string, groceryQuantity: string) => void
    completeRecipe?: () => void
}

const AddGrocery = (props: AddGroceryProps) => {
    let name = ""
    let quantity = ""

    const addGrocery = () => {
        props.addGrocery(name, quantity)
        
        name = ""
        quantity = ""
    }

    return (
        <form id="ingredientinputform" onSubmit={() => console.log("do nothing")}>
            <span>Ingredient</span>
            <input type="text" id='name' onChange={e => name = e.target.value} />

            Quantity
            <input type="text" id='quantity' name="quantity" onChange={e => {
                quantity = e.target.value
            }} />

            <button type="reset" onClick={addGrocery}>Add Grocery</button>
            {props.completeRecipe !== undefined ?
                <button onClick={props.completeRecipe}>Complete Recipe</button> : null}
        </form>
    )
}

export default AddGrocery